<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalLabel extends Model
{
    protected $table = 'additional_labels';

}

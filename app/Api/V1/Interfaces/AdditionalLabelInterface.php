<?php
namespace App\Api\V1\Interfaces;

interface AdditionalLabelInterface {
    public function storeAdditionalLabel($data, $title, $scanner_id, $guid = null);
    public function getGuidMaster();
    public function getGuidTicket();
    public function getParameter();
}
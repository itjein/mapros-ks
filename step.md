## check additional label
1. buat hasAdditionalLabel in node.php
2. apabila ada lakukan proses di dalam prses board => if($node->getStatus() == 'IN')
3. simpan data ke tabel additional label


step Panel :
 main controller -> store()
 -> runNode($parameter)


NODE PARAMETER

{
    "scanner_id":52,
    "scanner":{
        "id":52,"line_id":1,"lineprocess_id":52,"name":"SCANNER KS PANEL 2 + VIDEO + OP",
        "mac_address":"-","ip_address":"KS PANEL 2 + VIDEO + OP"
    },
    "id_type":"panel","unique_column":"guid_ticket","unique_id":"C56BFCEE-A1CF-4C38-BEE5-A6761D7463FD",
    "dummy_column":"ticket_no","dummy_id":"MAPNL015TEST1","guid_master":null,
    "guid_ticket":"C56BFCEE-A1CF-4C38-BEE5-A6761D7463FD","status":"OUT","judge":"OK","nik":"ZAKI",
    "board":{
        "id":830,"name":"TESTING","pwbno":"J7J-1039-00","pwbname":"OPERATION UNIT","process":"DM1","cavity":"12",
        "code":"YJ5306J00OT","side":"A"},"process":"15,52,17",
        "lineprocess":{
            "id":52,"name":"KS PANEL 2 + VIDEO + OP","type":1,"std_time":1,"endpoint_id":null,"join_qty":2,
            "column_settings":[
                {
                    "id":1,
                    "name":"board","dummy_column":"board_id","table_name":"boards","code_prefix":"YJ1",
                    "created_at":"2018-08-13 13:57:00","updated_at":"2023-11-16 15:17:20",
                    "level":4,
                    "pivot":{
                        "lineprocess_id":52,"column_setting_id":1
                        }
                    },
                    {
                        "id":2,"name":"panel","dummy_column":"ticket_no","table_name":"tickets","code_prefix":"MAPNL",
                        "created_at":"2018-08-13 13:58:00","updated_at":"2018-11-08 17:10:01","level":2,
                        "pivot":
                        {
                            "lineprocess_id":52,"column_setting_id":2
                        }
                    }
                ]
        },
        "step":{
            "id":1,"ticket_no":"MAPNL015TEST1","guid_master":null,"guid_ticket":"C56BFCEE-A1CF-4C38-BEE5-A6761D7463FD",
            "scanner_id":52,"status":"OUT","scan_nik":"ZAKI","judge":"OK","created_at":"2023-11-17 09:01:12",
            "updated_at":"2023-11-17 09:01:12","reworked_at":null
        },
        "model":[],"column_setting":[{"id":1,"name":"board","dummy_column":"board_id","table_name":"boards","code_prefix":"YJ1","created_at":"2018-08-13 13:57:00","updated_at":"2023-11-16 15:17:20","level":4,"pivot":{"lineprocess_id":52,"column_setting_id":1}},{"id":2,"name":"panel","dummy_column":"ticket_no","table_name":"tickets","code_prefix":"MAPNL","created_at":"2018-08-13 13:58:00","updated_at":"2018-11-08 17:10:01","level":2,"pivot":{"lineprocess_id":52,"column_setting_id":2}}],"modelname":"TESTING","lotno":null,"critical_parts":null,"parameter":{"board_id":"MAPNL015TEST1","nik":"ZAKI","ip":"KS PANEL 2 + VIDEO + OP","guid":null,"is_solder":false,"modelname":"TESTING","judge":"OK","symptom":[],"critical_parts":null,"locations":[],"isRework":false,"manual_content":[],"carton":null,"serial_number":null,"fifoMode":false,"qrPanel":"","sirius":"","pet":"","ldpe":""}}

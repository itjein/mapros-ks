<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdditionalTableWithAddScanneridAndLabelName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(Schema::hasTable('additional_labels')){
            Schema::table('additional_labels', function(Blueprint $table){
                if(Schema::hasColumn('additional_labels', 'title') == false) {
                    $table->string('title',100)->nullable();
                }
                if(Schema::hasColumn('additional_labels', 'scanner_id') == false) {
                    $table->integer('scanner_id')->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        if(Schema::hasTable('additional_labels')){
            Schema::table('additional_labels', function(Blueprint $table){
                if(Schema::hasColumn('additional_labels', 'title') == false) {
                    $table->dropColumn('title');
                }
                if(Schema::hasColumn('additional_labels', 'scanner_id') == false) {
                    $table->dropColumn('scanner_id');
                }
            });
        }
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardGenerateFileTable extends Migration
{
   public function up()
    {
        Schema::create('board_generate_file', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pwbname');
            $table->string('code');

            $table->integer('generated'); //it should be 0 or 1; boolean

            $table->string('nik', 30)->nullable();            
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::dropIfExists('board_generate_file');
    }
}
